package com.example.ioasysps.model

data class LoginAccount(
    val email: String,
    val password: String,
)
