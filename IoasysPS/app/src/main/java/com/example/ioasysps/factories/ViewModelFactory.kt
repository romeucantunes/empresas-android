package com.example.ioasysps.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.ioasysps.ui.dashboard.DashboardRepository
import com.example.ioasysps.ui.dashboard.DashboardViewModel
import com.example.ioasysps.ui.login.LoginRepository
import com.example.ioasysps.ui.login.LoginViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(
    private val repository: Any?,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(LoginRepository()) as T
        } else if (modelClass.isAssignableFrom(DashboardViewModel::class.java)) {
            return DashboardViewModel(DashboardRepository()) as T
        }
        throw IllegalArgumentException("ViewModel not found")
    }
}