package com.example.ioasysps.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ioasysps.model.Login
import com.example.ioasysps.model.LoginAccount
import kotlinx.coroutines.launch
import retrofit2.Response

class LoginViewModel(private val repository: LoginRepository) : ViewModel() {


    val loginResponse: MutableLiveData<Response<Login>> = MutableLiveData()

    fun sign_in(loginAccount: LoginAccount) {
        viewModelScope.launch {
            val response = repository.signIn(loginAccount)
            loginResponse.value = response
        }
    }
}