package com.example.ioasysps.ui.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ioasysps.model.EnterpriseResponse
import kotlinx.coroutines.launch
import retrofit2.Response

class DashboardViewModel(private val repository: DashboardRepository) : ViewModel(){

    val enterpriseResponse: MutableLiveData<Response<EnterpriseResponse>> = MutableLiveData()

    fun showEnterprises(token:String, uid:String, clientCode:String) {
        viewModelScope.launch {
            val response = repository.showEnterprises(token,uid,clientCode)
            enterpriseResponse.value = response
        }
    }
}