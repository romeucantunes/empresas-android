package com.example.ioasysps.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.ioasysps.R
import com.example.ioasysps.factories.ViewModelFactory
import com.example.ioasysps.model.LoginAccount
import com.example.ioasysps.ui.dashboard.DashboardActivity
import com.example.ioasysps.util.Constants
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initViewModels()
        initViews()
    }

    private fun initViews() {
        // TODO REMOVER ESSE AUXILIO
        findViewById<TextInputEditText>(R.id.et_email).setText("testeapple@ioasys.com.br")
        findViewById<TextInputEditText>(R.id.et_password).setText("12341234")

        findViewById<Button>(R.id.bt_login).setOnClickListener {
            showProgressBar(true)
            findViewById<TextInputLayout>(R.id.til_email).error = null
            findViewById<TextInputLayout>(R.id.til_password).error = null
            loginViewModel.sign_in(
                LoginAccount(
                    findViewById<TextInputEditText>(R.id.et_email).text.toString(),
                    findViewById<TextInputEditText>(R.id.et_password).text.toString()
                )
            )
        }
    }

    private fun showProgressBar(showing: Boolean) {
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)
        if (showing) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

    private fun initViewModels() {
        val repository = LoginRepository()
        val viewModelFactory = ViewModelFactory(repository)
        loginViewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)
        initObservers()
    }

    private fun initObservers() {
        loginViewModel.loginResponse.observe(this, { response ->
            if (response.isSuccessful) {
                showProgressBar(false)
                val intent  = Intent(this, DashboardActivity::class.java)
                intent.putExtra(Constants.ACCESS_TOKEN, response.headers()["access-token"].toString())
                intent.putExtra(Constants.UID, response.headers()["uid"].toString())
                intent.putExtra(Constants.CLIENT_CODE, response.headers()["client"].toString())
                startActivity(intent)
            } else {
                findViewById<TextInputLayout>(R.id.til_email).error = " "
                findViewById<TextInputLayout>(R.id.til_password).error =
                    "Credenciais informadas são inválidas, tente novamente"
                showProgressBar(false)
            }
        })
    }
}