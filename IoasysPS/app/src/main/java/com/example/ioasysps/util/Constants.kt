package com.example.ioasysps.util

class Constants {
    companion object{
        const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
        const val ACCESS_TOKEN = "ACCESS_TOKEN"
        const val UID = "UID"
        const val CLIENT_CODE = "CLIENT_CODE"
        const val ENTERPRISE_NAME ="ENTERPRISE_NAME"
        const val ENTERPRISE_DESCRIPTION ="ENTERPRISE_DESCRIPTION"
    }
}