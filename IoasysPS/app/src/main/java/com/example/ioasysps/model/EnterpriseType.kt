package com.example.ioasysps.model

import com.google.gson.annotations.SerializedName

data class EnterpriseType(
    val id: Long,
    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String
)
