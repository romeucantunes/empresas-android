package com.example.ioasysps.ui.dashboard

import android.opengl.Visibility
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ioasysps.R
import com.example.ioasysps.factories.ViewModelFactory
import com.example.ioasysps.model.EnterpriseResponse
import com.example.ioasysps.util.Constants

class DashboardActivity : AppCompatActivity() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var token: String
    private lateinit var uid: String
    private lateinit var clientCode: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        initViewModels()
    }

    fun initViews(){
        findViewById<RecyclerView>(R.id.rv_enterprise_list).visibility = View.VISIBLE
        findViewById<TextView>(R.id.tv_click_search).visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_search) {
            initObservers()
            initViews()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        val search = menu?.findItem(R.id.menu_search)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Pesquisar"
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // TODO Não tive tempo o suficiente para implementar o filtro do RV
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    private fun initViewModels() {
        token = intent.getStringExtra(Constants.ACCESS_TOKEN).toString()
        clientCode = intent.getStringExtra(Constants.CLIENT_CODE).toString()
        uid = intent.getStringExtra(Constants.UID).toString()
        val repository = DashboardRepository()
        val viewModelFactory = ViewModelFactory(repository)
        dashboardViewModel =
            ViewModelProvider(this, viewModelFactory).get(DashboardViewModel::class.java)
    }

    private fun initObservers() {
        dashboardViewModel.enterpriseResponse.observe(this, { response ->
            if (response.isSuccessful) {
                val enterprises : EnterpriseResponse = response.body()!!
                initAdapter(enterprises)
                showProgressBar(false)
            }
        })
        dashboardViewModel.showEnterprises(token, uid, clientCode)
        showProgressBar(true)
    }

    private fun initAdapter(enterprises : EnterpriseResponse) {
        findViewById<RecyclerView>(R.id.rv_enterprise_list).layoutManager =
            LinearLayoutManager(this)
        findViewById<RecyclerView>(R.id.rv_enterprise_list).adapter = EnterprisesAdapter(enterprises,this)
    }

    private fun showProgressBar(showing: Boolean) {
        val progressBar = findViewById<ProgressBar>(R.id.progressBar)
        if (showing) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }
}