package com.example.ioasysps.model

import com.google.gson.annotations.SerializedName

data class EnterpriseResponse(
    @SerializedName("enterprises")
    var enterprises: List<Enterprise>,
)
