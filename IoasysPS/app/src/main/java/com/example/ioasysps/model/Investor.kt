package com.example.ioasysps.model

import com.google.gson.annotations.SerializedName

data class Investor(
        val id: Long,
        @SerializedName("investor_name")
        val investorName: String,
        val email: String,
        val city: String,
        val country: String,
        val balance: Double,
        val photo: Any? = null,
        val portfolio: Portfolio,
        @SerializedName("portfolio_value")
        val portfolioValue: Double,
        @SerializedName("first_access")
        val firstAccess: Boolean,
        @SerializedName("super_angel")
        val superAngel: Boolean
)