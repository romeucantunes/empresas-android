package com.example.ioasysps.model

data class Login(
    val investor: Investor,
    val enterprise: Any? = null,
    val success: Boolean
)
