package com.example.ioasysps.ui.dashboard

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ioasysps.R
import com.example.ioasysps.model.Enterprise
import com.example.ioasysps.model.EnterpriseResponse
import com.example.ioasysps.ui.enterprise.EnterpriseActivity
import com.example.ioasysps.util.Constants

class EnterprisesAdapter(private val data :  EnterpriseResponse,  val context: Context) :
    RecyclerView.Adapter<EnterprisesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterprisesViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.card_enterprises_layout, parent, false
        )
        return EnterprisesViewHolder(itemView, context)
    }

    override fun onBindViewHolder(holder: EnterprisesViewHolder, position: Int) {
        holder.bind(data.enterprises[position])
    }

    // TODO TENTATIVA FALHA DE REALIZAR O FILTRO
//    fun filter(search: String){
//        var filteredEnterprises : List<Enterprise> = emptyList()
//        if(!search.isEmpty()){
//            for(enterprise in data.enterprises){
//                if(enterprise.enterpriseName.contains(search, ignoreCase = true)){
//                    filteredEnterprises.plus(enterprise)
//                }
//            }
//        }else {
//            filteredEnterprises = data.enterprises
//        }
//    }


    override fun getItemCount() = data.enterprises.size
}

class EnterprisesViewHolder(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView) {
    private val context = context
    private val tvName = itemView.findViewById<TextView>(R.id.tv_name)
    private val tvType = itemView.findViewById<TextView>(R.id.tv_type)
    private val tvCountry = itemView.findViewById<TextView>(R.id.tv_country)


    fun bind(data: Enterprise) {
        itemView.setOnClickListener {
            context.startActivity(
                Intent(context, EnterpriseActivity::class.java)
                    .putExtra(Constants.ENTERPRISE_NAME, data.enterpriseName)
                    .putExtra(Constants.ENTERPRISE_DESCRIPTION, data.description)
            )
        }

        tvName.text = data.enterpriseName
        tvType.text = data.enterpriseType.enterpriseTypeName
        tvCountry.text = data.country
    }
}
