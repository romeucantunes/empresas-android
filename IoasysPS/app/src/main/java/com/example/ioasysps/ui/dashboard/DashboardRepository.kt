package com.example.ioasysps.ui.dashboard

import com.example.ioasysps.api.RetrofitInstance
import com.example.ioasysps.model.EnterpriseResponse
import retrofit2.Response

class DashboardRepository {

    suspend fun showEnterprises(token:String, uid:String, clientCode:String): Response<EnterpriseResponse> {
        return RetrofitInstance.dashboardService.showEnterprises(token,uid,clientCode)
    }
}