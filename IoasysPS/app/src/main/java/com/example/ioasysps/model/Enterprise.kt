package com.example.ioasysps.model

import com.google.gson.annotations.SerializedName

data class Enterprise(
    val id: Long,
    @SerializedName("email_enterprise")
    val emailEnterprise: String,
    val facebook: String,
    val twitter: String,
    val linkedin: String,
    val phone: String,
    @SerializedName("own_enterprise")
    val ownEnterprise: Boolean,
    @SerializedName("enterprise_name")
    val enterpriseName: String,
    val photo: String,
    val description: String,
    val city: String,
    val country: String,
    val value: Long,
    @SerializedName("share_price")
    val sharePrice: Double,
    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseType
)
