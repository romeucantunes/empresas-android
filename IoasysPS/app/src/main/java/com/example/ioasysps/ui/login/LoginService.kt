package com.example.ioasysps.ui.login

import com.example.ioasysps.model.Login
import com.example.ioasysps.model.LoginAccount
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService {

    @POST("users/auth/sign_in")
    suspend fun signIn(@Body loginAccount: LoginAccount): Response<Login>
}