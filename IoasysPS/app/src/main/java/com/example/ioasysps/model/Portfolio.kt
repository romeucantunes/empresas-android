package com.example.ioasysps.model

import com.google.gson.annotations.SerializedName

data class Portfolio(
    @SerializedName("enterprises_number")
    val enterprisesNumber: Long,
    val enterprises: List<Any?>
)
