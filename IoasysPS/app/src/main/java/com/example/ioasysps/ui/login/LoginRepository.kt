package com.example.ioasysps.ui.login

import com.example.ioasysps.api.RetrofitInstance
import com.example.ioasysps.model.Login
import com.example.ioasysps.model.LoginAccount
import retrofit2.Response

class LoginRepository {

    suspend fun signIn(loginAccount: LoginAccount): Response<Login> {
        return RetrofitInstance.loginService.signIn(loginAccount)
    }
}