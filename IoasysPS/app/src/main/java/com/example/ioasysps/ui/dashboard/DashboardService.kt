package com.example.ioasysps.ui.dashboard

import com.example.ioasysps.model.EnterpriseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface DashboardService {

    @GET("enterprises/")
    suspend fun showEnterprises(
        @Header("access-token") token: String,
        @Header("uid") id: String,
        @Header("client") clientCode: String
    ): Response<EnterpriseResponse>
}