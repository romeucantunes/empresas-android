package com.example.ioasysps.ui.enterprise

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.ioasysps.R
import com.example.ioasysps.util.Constants

class EnterpriseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.enterprise_activity)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.title = intent.getStringExtra(Constants.ENTERPRISE_NAME)
        initView()
    }

    fun initView() {
        findViewById<TextView>(R.id.tv_enterprise_description).text =
            intent.getStringExtra(Constants.ENTERPRISE_DESCRIPTION)
    }

}