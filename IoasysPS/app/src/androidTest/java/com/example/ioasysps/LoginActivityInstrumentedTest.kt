package com.example.ioasysps

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.ioasysps.ui.login.LoginActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class LoginActivityInstrumentedTest {

    @Rule
    @JvmField
    public var activityScenarioRule = ActivityScenarioRule(LoginActivity::class.java)


    // Simple instrumented test to check if edit text is on the screen
    @Test
    fun emailIsDisplayed() {
        Espresso.onView(ViewMatchers.withId(R.id.et_email))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}